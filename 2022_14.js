/* -------------------------------------  в каталоге слева - открытие списков подборок ---------------------------------- */
$(document).ready(function() {
	$(".col_podborki .group .all").click(function(){
		$(this).parent().find("a").removeClass("hidden");
		$(this).addClass("hidden");
		return false;
	});
});



/* -------------------------------------  открытие пунктов мобильного меню каталога (по материалу, по отделке и тд) ---------------------------------- */
$(document).ready(function() {
	$(".menutable .menutable_name").click(function() {
		//если он был открыт, просто закрываем
		if ($(this).hasClass("opened")) {
			$(this).removeClass("opened");
			data_open_class = $(this).attr("data-open-class");
			$("."+data_open_class).removeClass("opened");
		}
		else{
			//закрываем все пункты
			$(".menutable .menutable_name").removeClass("opened");
			$(".menutable .hide").removeClass("opened");
			//открываем текущий пункт
			$(this).addClass("opened");
			data_open_class = $(this).attr("data-open-class");
			$("."+data_open_class).addClass("opened");
		}
	});
});




/* ---------------------------------------------------  закрытие меню по крестику ---------------------------------------- */
$(document).ready(function() {
	$(".menu_catalog_close").on("click", function() {
		$(".menu_catalog .megamenu-li").removeClass("show");
		$(".menu_catalog .megamenu").removeClass("show");
		$(".menu_catalog #dropdown_catalog").attr("aria-expanded", "false");
		return false;
	});
});


/* ---------------------------------------------------  живой поиск - открытие/закрытие поисковой строки ---------------------------------------- */
/* ---------------------------------------------------  for mobile ---------------------------------------- */
$(document).ready(function(){
	
	$(".search_openclose.mobile").click(function() {
		elem = $(".topsearch .form");
		if (elem.hasClass("hide")) elem.removeClass("hide");
		else elem.addClass("hide");
	});
	
	// при клике за пределами скрываем
	$(document).click(function(event) {
		if ($(event.target).closest(".topsearch").length) return;
		if ($(event.target).closest(".search_openclose.mobile").length) return;
		$(".topsearch .form").addClass("hide")
	});

});

/* ---------------------------------------------------  for desktop ---------------------------------------- */
$(document).ready(function(){
	
	$(".search_openclose.desktop").click(function() {
		elem = $(".topsearch2 .form");
		if (elem.hasClass("hide")) elem.removeClass("hide");
		else elem.addClass("hide");
	});
	
	// при клике за пределами скрываем
	$(document).click(function(event) {
		if ($(event.target).closest(".topsearch2").length) return;
		if ($(event.target).closest(".search_openclose.desktop").length) return;
		$(".topsearch2 .form").addClass("hide")
	});

});



/* ---------------------------------------------------  живой поиск - результаты ----------------------------------------------------------- */
/* ---------------------------------------------------  for mobile ---------------------------------------- */
$(function(){
  $("#search").keyup(function(){
     var search = $("#search").val();
     $.ajax({
       type: "POST",
       url: "livesearch.html",
       data: {"search": search},
       cache: false,                                
       success: function(response){
          $("#resSearch").html(response);
       }
     });
     return false;
   });
});

/* ---------------------------------------------------  for desktop ---------------------------------------- */
$(function(){
  $("#search2").keyup(function(){
     var search = $("#search2").val();
     $.ajax({
       type: "POST",
       url: "livesearch.html",
       data: {"search": search},
       cache: false,                                
       success: function(response){
          $("#resSearch2").html(response);
       }
     });
     return false;
   });
});

// при клике за пределами формы и выдачи очищаем выдачу
// это стало не нужным, когда строка поиска стала скрываемая
/*
$(document).click(function(event) {
    if ($(event.target).closest("#resSearch").length) return;
	if ($(event.target).closest("#search").length) return;
	$("#resSearch").empty();
});
*/


/* ---------------------------------------------------  рейтинг карточки товара (звездочки) ----------------------------------------------------------- */
$(document).ready(function(){
	$(".cardrating .m span").hover(function(){
		rate = $(this).parent().attr("data-rating");
		$(".cardrating .m").addClass("hover_noactive");
		if (rate == "1") $(".cardrating .m1").addClass("hover_active");
		if (rate == "2") $(".cardrating .m1, .cardrating .m2").addClass("hover_active");
		if (rate == "3") $(".cardrating .m1, .cardrating .m2, .cardrating .m3").addClass("hover_active");
		if (rate == "4") $(".cardrating .m1, .cardrating .m2, .cardrating .m3, .cardrating .m4").addClass("hover_active");
		if (rate == "5") $(".cardrating .m1, .cardrating .m2, .cardrating .m3, .cardrating .m4, .cardrating .m5").addClass("hover_active");
	}, function(){
		$(".cardrating .m").removeClass("hover_active");
		$(".cardrating .m").removeClass("hover_noactive");
	});
	
	$(".cardrating .m span").click(function(){
		// сохраняем оценку через jquery селектор в базу данных
		alert("Спасибо. Ваша оценка будет учтена при подсчёте рейтинга этой кухни");
	});
});



/* ---------------------------------------------------  главная страница - открытие видео по заглушке ----------------------------------------------------------- */
$(document).ready(function() {
	$('.frommaker .video_open a').click( function(){
		$(".frommaker .hide_no").css("display", "none");
		$(".frommaker .hide").css("display", "block");
		return false;
	});
});	


/* ---------------------------------------------------  кнопка открытия фильтра каталога на мобильной версии ----------------------------------------------------------- */
$(document).ready(function() {
	$('.filter2022 .btn_outer_filterhidemobile a').click( function(){
		elem = $(".filter2022 .filterall");
		if (elem.hasClass("hide")) elem.removeClass("hide");
		else elem.addClass("hide");
		return false;
	});
});	




// галерея изображений в карточке товара
$(document).ready(function() {
	$('.card2022').photobox('a.loaded',{ time:0 });
	$('.card2022').photobox('a.phtbx',{ time:0 });
	$('#gallery').photobox('a.loaded',{ time:0 });
	$('.page_reviews').photobox('a.loaded',{ time:0 }); 
});	
//$('a.phtbx').photobox({ thumbs:true });


// сортировка в каталоге
function sort_change(){
	var val = $("#sort").val();
	var cur = window.location.href;
	var cur2 = cur.split('?')[0]; 
	if (!(val == "0")) newhref = cur2 + '?sort=' + val;
	else newhref = cur2;
	window.location.href = newhref; 		
}
	

// тема для универсальной заявки MODAL_SHORT
function set_feedback_theme_2022(theme, llink){
	$("#MODAL_SHORT h2").html(theme);
	$("#MODAL_SHORT input[name='feed_zag']").attr("value", theme);
}
		
	$(document).ready(function(){

		// Блок Популярные вопросы на главной странице
		$('.accordeon .acc-head').on('click', f_acc);
		
		
		function f_acc(){
		//скрываем все кроме того, что должны открыть
		  $('.accordeon .acc-body').not($(this).next()).slideUp(1000);
		  $('.accordeon .acc-head').parent().removeClass("opened");
		// открываем или скрываем блок под заголовком, по которому кликнули
			//$(this).next().slideToggle(200);
			$(this).next().slideToggle('normal',function(){
				if ($(this).is(':hidden')) {
					$('.accordeon .acc-head').parent().removeClass("opened");
				} else {
					$(this).parent().addClass("opened");
				}
				return false;
			});
		}
		$('.accordeon .item.first .acc-head').trigger("click");
		
		
		
		
		// maskedinput - маска ввода номера телефона в input'ах
		jQuery(function($){
			// $("#feed_calc_phone2").mask("+7 (999) 999-99-99");
			// $("#feedback_gift_phone").mask("+7 (999) 999-99-99");
			$(".phonemask").mask("+7 (999) 999-99-99"); // универсально
		});
		
		
		
		// на главной странице блок "Подарок на выбор"
		$('body').on('click', '.r_check > div', function(){
			$('.r_check > div').removeClass('active');
			$(this).addClass("active");
			$("#feedback_gift_what").val( $(this).find("p").html() );
			
		});
		
		
		
		
		$('a.modalopen_video_1').click( function(event){
			event.preventDefault(); // выключaем стaндaртную рoль элементa
			$('#overlay').fadeIn(400, // снaчaлa плaвнo пoкaзывaем темную пoдлoжку
				function(){ // пoсле выпoлнения предъидущей aнимaции
					$('.modal_form_video_1') 
						.css('display', 'block') // убирaем у мoдaльнoгo oкнa display: none;
						.animate({opacity: 1, top: '50%'}, 200); // плaвнo прибaвляем прoзрaчнoсть oднoвременнo сo съезжaнием вниз
			});
		});
	
	
	
	});
	

// проверка калькулятора "Онлайн расчет Вашей кухни" на главной
function check_feedback_calc2(){
	var feed_calc_phone=$("#feed_calc_phone2").val();
	if (feed_calc_phone==""){
		$("#feed_calc_error2 .send_error").addClass("show");
		return false;
	}
	else {
		yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}
// сброс ошибки при фокусе на поле телефона
$(document).ready(function(){
	$("#feed_calc_phone2").focus( function(){
		$("#feed_calc_error2 .send_error").removeClass("show");
	})
});




function check_feedback_gift(){
	var feedback_gift_phone=$("#feedback_gift_phone").val();
	if (feedback_gift_phone==""){
		alert("Необходимо указать номер телефона");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}


function check_feedback_consult(){
	var feedback_consult_phone=$("#feedback_consult_phone").val();
	if (feedback_consult_phone==""){
		alert("Необходимо указать номер телефона");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}

function check_callme(){
	var callme_mail=$("#callme_mail").val();
	if (callme_mail==""){
		alert("Необходимо указать контактные данные");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}

function check_yourask(){
	var yourask_text=$("#yourask_text").val();
	if (yourask_text==""){
		alert("Необходимо указать Ваш вопрос");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}

function check_elseask(){
	var elseask_email=$("#elseask_email").val();
	if (elseask_email==""){
		alert("Необходимо указать номер телефона или email");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}






		/*  -------------------------------------- заносит параметры калькулятора в скрытое поле - НЕМОДАЛЬНЫЙ КАЛЬК ДЛЯ НОВОЙ ГЛАВНОЙ -------------------------------------------- */
function calc_params_set2(){
		
		var a = $('.onlineraschet .form_active').attr('id');
		
		if(a == 1){
			var form_type = "Прямая кухня";
		}else if(a == 2){
			var form_type = "Г-образная левая кухня";
		}else if(a == 3){
			var form_type = "Г-образная правая кухня";
		}else{
			var form_type = "П-образная кухня";
		}
		
		var IA = "Сторона а: " + $('.onlineraschet #onlineraschet_IA').val() + " / ";
		var IB = "Сторона b: " + $('.onlineraschet #onlineraschet_IB').val() + " / ";
		var IC = "Сторона c: " + $('.onlineraschet #onlineraschet_IC').val() + " / ";
		var or_material = "Материал фасада: " + $(".onlineraschet select.or_material option:selected").text() + " / ";
		var or_polki = "Верхние полки: " + $(".onlineraschet select.or_polki option:selected").text() + " / ";
		var prices = "Расчетная стоимость: " + $('.onlineraschet .prices').html();
		
		$('#feed_calc_params2').val(form_type + " / " + IA + IB + IC + or_material + or_polki + prices);
		
}



function check_feedback_10kuhon(){
	var feedback_10kuhon_phone=$("#feedback_10kuhon_phone").val();
	if (feedback_10kuhon_phone==""){
		alert("Необходимо указать номер телефона");
		return false;
	}
	else {
		//yaCounter38722450.reachGoal('feedsend');
		return true;
	}
}


$(document).ready(function(){

	$(document).ready(function() {
		$('body').on('click', '.onlineraschet .col_l .items .item', function(){
			$('.onlineraschet .col_l .items .item').removeClass('form_active');
			$(this).addClass('form_active');

			var a = $(this).attr('id');
			$('.onlineraschet .inp_none.storona').css('display', 'none');
			if(a == "calc_forma_1"){
				$('#onlineraschet_B').css('display', 'block');
				$('#onlineraschet_IB').css('display', 'block');
			}else if(a == "calc_forma_2"){
				$('#onlineraschet_A').css('display', 'block');
				$('#onlineraschet_IA').css('display', 'block');
				$('#onlineraschet_B').css('display', 'block');
				$('#onlineraschet_IB').css('display', 'block');
			}else if(a == "calc_forma_3"){
				$('#onlineraschet_B').css('display', 'block');
				$('#onlineraschet_IB').css('display', 'block');
				$('#onlineraschet_C').css('display', 'block');
				$('#onlineraschet_IC').css('display', 'block');
			}else{
				$('#onlineraschet_A').css('display', 'block');
				$('#onlineraschet_IA').css('display', 'block');
				$('#onlineraschet_B').css('display', 'block');
				$('#onlineraschet_IB').css('display', 'block');
				$('#onlineraschet_C').css('display', 'block');
				$('#onlineraschet_IC').css('display', 'block');
			}
		})
	});
	
	$(document).ready(function() {
		$('body').on('click', '.onlineraschet .or_material option', function(){
			var modla = $('.onlineraschet .or_material').val();
			//document.getElementById('modelvb').value = '0';​​​​​​​​​​
			$('.onlineraschet .or_model option').css('display', 'none');
			if(modla == "1"){
				$('.or_model .derervo').css('display', 'block');
				document.getElementById('modelvb').options[0].selected = true;
				//document.getElementById("derervoo").selected = true;
			}else if(modla == "2"){
				$('.or_model .emal').css('display', 'block');
				document.getElementById('modelvb').options[0].selected = true;
				//document.getElementById("emalo").selected = true;
			}else if(modla == "3"){
				$('.or_model .shpon').css('display', 'block');
				document.getElementById('modelvb').options[0].selected = true;
				//document.getElementById("shpono").selected = true;
			}else if(modla == "4"){
				$('.or_model .emalpatina').css('display', 'block');
				document.getElementById('modelvb').options[0].selected = true;
				//document.getElementById("emalpatinao").selected = true;
			}else if(modla == "5"){
				$('.or_model .emalshpon').css('display', 'block');
				document.getElementById('modelvb').options[0].selected = true;
				//document.getElementById("emalshpono").selected = true;
			}
			onlineraschet_raschet();
		})
	});

	function onlineraschet_raschet(){

		var a = $('.onlineraschet .form_active').attr('id');
		if(a == "calc_forma_1"){
			var metr = parseFloat($('#onlineraschet_IB').val().replace(',','.')); 
		}else if(a == "calc_forma_2"){
			var metr = parseFloat($('#onlineraschet_IB').val().replace(',','.')) + parseFloat($('#onlineraschet_IA').val().replace(',','.')); 
		}else if(a == "calc_forma_3"){
			var metr = parseFloat($('#onlineraschet_IB').val().replace(',','.')) + parseFloat($('#onlineraschet_IC').val().replace(',','.')); 
		}else{
			var metr = parseFloat($('#onlineraschet_IB').val().replace(',','.')) + parseFloat($('#onlineraschet_IC').val().replace(',','.')) + parseFloat($('#onlineraschet_IA').val().replace(',','.')); 
		}


		var mater = $('.onlineraschet .or_material').val();
		/*Массив дерева*/								if(mater == '1'){mater = 67.14; zor1 = '<i class="fa fa-rocket" aria-hidden="true"></i> Изготовим за 5 дней!'; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 5 лет!';}
		/*МДФ Эмаль*/							else if(mater == '2'){mater = 56.6; zor1 = '<i class="fa fa-rocket" aria-hidden="true"></i> Изготовим за 5 дней!'; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 5 лет!';}
		/*МДФ Шпон*/							else if(mater == '3'){mater = 58.7; zor1 = ''; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 10 лет!';}
		/*МДФ Эмаль патина*/							else if(mater == '4') {mater = 58.75; zor1 = ''; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 10 лет!';}
		/*МДФ комбо: эмаль и шпон*/	else if(mater == '5'){mater = 57; zor1 = '<i class="fa fa-rocket" aria-hidden="true"></i> Изготовим за 5 дней!'; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 5 лет!';}
		/*мдф Эмаль патина/			else if(mater == '6'){mater = 32.7; zor1 = ''; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 10 лет!';}*/	
		/*Массив дерева Италия/			else if(mater == '7'){mater = 50; zor1 = ''; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 10 лет!';}*/
		/*Акриловый пластик Alvic Luxe/	else if(mater == '4'){mater = 35; zor1 = '<i class="fa fa-rocket" aria-hidden="true"></i> Изготовим за 5 дней!'; zor2 = '<i class="fa fa-shield" aria-hidden="true"></i> Даем гарантию 5 лет!';}*/
        
		var modl = $('.onlineraschet .or_model').val();
		

		var polki = $('.onlineraschet .or_polki').val();
		if(modl > 0){
		var total = metr*modl;
		}else {
		var total = metr*mater;
		}

		if(polki == '0'){
			total = total/2;
		}

		if(total < 700){
			total = total*1000;
		}

		// 03/07/2021 подняли цены
		total = parseInt(total*1.1);

		$('.onlineraschet .zor_1').html(zor1);
		$('.onlineraschet .zor_2').html(zor2);

		if (total) $('.onlineraschet .prices').html(total + " ₽");
		//else $('.onlineraschet .prices').html("недостаточно данных");
		else $('.onlineraschet .prices').html("0 ₽");
		
		
		if(total > 0){
			$('.order_online').fadeIn();
			$('.this_fio').fadeIn();
			$('.in_price_or').fadeIn();
			$('.this_phone').fadeIn();
			$('.this_email').fadeIn();
			$('.this_comment').fadeIn();
			$('.this_file').fadeIn();
		}else{
			$('.order_online').fadeOut();
			$('.in_price_or').fadeOut();
			$('.this_fio').fadeOut();
			$('.this_phone').fadeOut();
			$('.this_comment').fadeOut();
			$('.this_email').fadeOut();
			$('.this_file').fadeOut();
		}
		
		//$('.onlineraschet #feed_calc_params').val("");
	}

	$(document).ready(function() {
		$('body').on('keyup', '.onlineraschet .or_inp', function(){
			onlineraschet_raschet();
		})
		$('body').on('keyup', '.onlineraschet .dima_baga' , function(){
			var _href = '.onlineraschet .or_n_left';
	        $("html, body").animate({scrollTop: $(_href).offset().top+"px"}, 300);
	        return false;
		})
	});

	$(document).ready(function() {
		$('body').on('change', '.onlineraschet .or_material', function(){
			onlineraschet_raschet();
		})
	});
	
	$(document).ready(function() {
		$('body').on('change', '.onlineraschet .or_model', function(){
			onlineraschet_raschet();
		})
	});
	
	$(document).ready(function() {
		$('body').on('click', '.onlineraschet .col_l .items .item', function(){
			onlineraschet_raschet();
		})
	});

	$(document).ready(function() {
		$('body').on('change', '.onlineraschet .or_polki', function(){
			onlineraschet_raschet();
		})
	});


/*Calltouch code*/
(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)},m=typeof Array.prototype.find === 'function',n=m?"init-min.js":"init.js";s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/"+n+"?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","ewwr1rnz");


jQuery(document).ready(function(){ 
    jQuery('form').on('submit', function() { 
        
        try {
        var m = jQuery(this);
        var fio = m.find('input[name*="name"]').val(); 
        var phone = m.find('input[name*="phone"]').val(); 
        var email = m.find('input[name*="email"],input#elseask_email').val();
        var ct_site_id = '52455';
        var sub = 'Заявка с '+ location.hostname;
        var ct_data = {             
            fio: fio,
            email: email,
            phoneNumber: phone,
            requestUrl: location.href,
            subject: sub,
            sessionId: window.call_value 
        };  
        if ((!!phone||!!email)&& window.ct_snd_flag != 1){
		window.ct_snd_flag = 1; setTimeout(function(){ window.ct_snd_flag = 0; }, 20000);
            jQuery.ajax({  
              url: 'https://api-node12.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
              dataType: 'json', type: 'POST', data: ct_data, async: false
            }); 
        }
        
        } catch(e) { console.log(e); }
    });
    });
/*Calltouch code*/
    
});